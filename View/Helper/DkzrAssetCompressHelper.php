<?php
App::uses('AssetCompressHelper', 'AssetCompress.View/Helper');
App::uses('AssetCompressController', 'DkzrUtils.Lib');

/**
 * DkzrAssetCompressHelper Helper.
 *
 * Extension of the AssetCompressHelper. It builds the requested files when
 * it doesn't exist.
 *
 */
class DkzrAssetCompressHelper extends AssetCompressHelper {

/**
 * Create a CSS file. Will generate link tags
 * for either the dynamic build controller, or the generated file. It will
 * build the generated file if it does not exist.
 *
 * This function will only work when debug=0 and the 'raw' option is not set,
 * otherwise the original AssetCompress method is used.
 *
 * @param string $file A build target to include.
 * @param array $options An array of options for the stylesheet tag.
 * @throws RuntimeException
 * @return A stylesheet tag
 */
	public function css($file, $options = array()) {
		if (0 == Configure::read('debug') && empty($options['raw'])) {
			// check if file exists
			$target = $this->_addExt($file, '.css');
			$url = $this->url($target);
			if (!file_exists(WWW_ROOT . $url) || !is_file(WWW_ROOT . $url)) {
				// build it
				AssetCompressController::buildFile($target);
			}
			return $this->Html->css($url, $options);
		}

		return parent::css($file, $options);
	}

/**
 * Create a script tag for a script asset. Will generate script tags
 * for either the dynamic build controller, or the generated file. It will
 * build the generated file if it does not exist.
 *
 * This function will only work when debug=0 and the 'raw' option is not set,
 * otherwise the original AssetCompress method is used.
 *
 * @param string $file A build target to include.
 * @param array $options An array of options for the script tag.
 * @throws RuntimeException
 * @return A script tag
 */
	public function script($file, $options = array()) {
		if (0 == Configure::read('debug') && empty($options['raw'])) {
			// check if file exists
			$target = $this->_addExt($file, '.js');
			$url = $this->url($target);
			if (!file_exists(WWW_ROOT . $url) || !is_file(WWW_ROOT . $url)) {
				// build it
				AssetCompressController::buildFile($target);
			}
			return $this->Html->script($url, $options);
		}

		return parent::script($file, $options);
	}
}
