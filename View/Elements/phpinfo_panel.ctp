<?php
/**
 * Phpinfo Panel Element
 *
 * PHP 5
 */
?>
<style type="text/css">
#phpinfo-tab .panel-content-data { height: 100%; }
</style>
<iframe id="panel_phpinfo_content" width="95%" height="99%" frameborder="0" src="<?php printf('data:text/html;charset=utf-8,%s', rawurlencode($content)); ?>"></iframe>
