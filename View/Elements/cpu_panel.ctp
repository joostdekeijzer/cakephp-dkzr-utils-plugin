<?php
/**
 * Cpu Panel Element
 *
 * PHP 5
 */
$this->Number = $this->Helpers->load('Number');
?>
<div class="debug-info">
	<h2><?php echo __d('dkzr_utlis', 'CPU Usage'); ?></h2>
	<?php
	$headers = array(__d('dkzr_utlis', 'Measured over last'), __d('dkzr_utlis', 'CPU %'));

	$rows = array();
	if (isset($content['error'])):
		$rows[] = array('-', $content['error']);
	else:
		foreach ($content as $key => $value):
			$rows[] = array($key, $this->Number->toPercentage($value, 0, array('multiply' => true)));
		endforeach;
	endif;

	echo $this->Toolbar->table($rows, $headers);
	?>
</div>
