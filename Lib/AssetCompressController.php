<?php
/**
 * Controll AssetCompress Plugin
 */

App::uses('AssetConfig', 'AssetCompress.Lib');
App::uses('AssetCompiler', 'AssetCompress.Lib');
App::uses('AssetCache', 'AssetCompress.Lib');
App::uses('Folder', 'Utility');

class AssetCompressController {
	static protected $config;
	static protected $compiler;
	static protected $cache;

	protected static function setup() {
		if (!class_exists('AssetConfig')) {
			return false;
		}

		if (!isset(self::$config)) {
			self::$config = AssetConfig::buildFromIniFile();
			self::$compiler = new AssetCompiler(self::$config);
			self::$cache = new AssetCache(self::$config);
		}

		return true;
	}

	public static function build($force = false) {
		if ( !self::setup() ) {
			return false;
		}

		$output = '';
		if ($force) {
			$output .= self::clear() . '<br>';
		}

		$output .= 'Build asset files<br>';


		foreach (array('js', 'css') as $group) {
			foreach (self::$config->targets($group) as $target) {
				$name = self::$cache->buildFileName($target);
				if (self::$cache->isFresh($target)) {
					// still fresh
					$output .= sprintf(' - %s file %s is still fresh<br>', $group, $target);
				} else {
					self::$cache->setTimestamp($target, 0);
					if (true === ($result = self::buildFile($target))) {
						$output .= sprintf(' - parsed &amp; written %s file %s<br>', $group, $target);
					} else {
						$output .= sprintf('*** Error parsing %s file %s***<br>%s<br><br>', $group, $target, $result);
					}
				}
			}
		}
		return $output;
	}

	public static function buildFile($target) {
		if ( !self::setup() ) {
			return false;
		}

		try {
			$contents = self::$compiler->generate($target);
			self::$cache->write($target, $contents);
			return true;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public static function clear() {
		if ( !self::setup() ) {
			return false;
		}

		$output = 'Clear asset files<br>';
		if (is_dir(CACHE . AssetConfig::CACHE_CONFIG)) {
			$files = array_diff(scandir(CACHE . AssetConfig::CACHE_CONFIG), array('.', '..'));
			foreach ($files as $file) {
				if (is_file(CACHE . AssetConfig::CACHE_CONFIG . '/' . $file)) {
					unlink(CACHE . AssetConfig::CACHE_CONFIG . '/' . $file);
				}
			}
		}
		foreach (array('js', 'css') as $group) {
			$path = self::$config->cachePath($group);
			$targets = self::$config->targets($group);
			$dir = new DirectoryIterator($path);
			foreach ($dir as $file) {
			$name = $base = $file->getFilename();
			if (in_array($name, array('.', '..'))) {
				continue;
			}
			// timestampped files.
			if (preg_match('/^(.*)\.v\d+(\.[a-z]+)$/', $name, $matches)) {
				$base = $matches[1] . $matches[2];
			}
			if (in_array($base, $targets)) {
				$output .= ' - deleting ' . $path . $name . '<br>';
				unlink($path . $name);
				continue;
			}
		}
		}

		$output .= ' - clear build timestamp<br>';
		AssetConfig::clearBuildTimeStamp();

		return $output;
	}
}
