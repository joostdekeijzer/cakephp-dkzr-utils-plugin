<?php
/**
 * Phpinfo Panel
 *
 * Provides information about your PHP and CakePHP environment to assist with debugging.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 */

App::uses('DebugPanel', 'DebugKit.Lib');

/**
 * Class PhpinfoPanel
 *
 */
class PhpinfoPanel extends DebugPanel {
	public $plugin = 'DkzrUtils';

/**
 * beforeRender - Get necessary data about environment to pass back to controller
 *
 * @param Controller $controller
 * @return array
 */
	public function beforeRender(Controller $controller) {
		parent::beforeRender($controller);

		ob_start();
		phpinfo();
		$return = preg_replace('/\s+/', ' ', ob_get_clean());

		return $return;
	}
}