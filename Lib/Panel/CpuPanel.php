<?php
/**
 * CPU Panel
 *
 * Provides information about your PHP and CakePHP environment to assist with debugging.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 */

App::uses('DebugPanel', 'DebugKit.Lib');

/**
 * Class CpuPanel
 *
 */
class CpuPanel extends DebugPanel {
	public $plugin = 'DkzrUtils';

/**
 * beforeRender - Get necessary data about environment to pass back to controller
 *
 * @param Controller $controller
 * @return array
 */
	public function beforeRender(Controller $controller) {
		parent::beforeRender($controller);

		if (function_exists('sys_getloadavg')) {
			$return = array_combine(
				array(
					__d('dkzr_utlis', '1 minute'),
					__d('dkzr_utlis', '5 minutes'),
					__d('dkzr_utlis', '15 minutes')
				),
				sys_getloadavg()
			);
		} else {
			$return = array(
				'error' => __d('dkzr_utlis', 'The get system load average function `sys_getloadavg` is not available on this sytem.'),
			);
		}

		return $return;
	}
}
