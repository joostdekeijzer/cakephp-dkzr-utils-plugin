<?php
/**
 * Facebook Graph Api Client
 * 
 * https://developers.facebook.com/docs/graph-api/
 * https://developers.facebook.com/docs/graph-api/reference/user/
 */
App::uses('HttpSocket', 'Network/Http');

class FacebookGraphApiClient {
/**
 * Settings for this class
 *
 * @var array
 */
	public $settings = array();

	protected $__apiUrl = "https://graph.facebook.com";
	protected $__apiVersion = 'v2.9';

	protected $_client;

	protected $_cacheSettings;

	public $resetCache = false;

	public function __construct($settings = array()) {
		$this->settings = Hash::merge(array('cache_config' => 'default'), $settings);

		if ( array_key_exists('apiVersion', $settings) && ! is_null( $settings['apiVersion'] ) ) {
			if ( empty( $settings['apiVersion'] ) ) {
				$this->__apiVersion = '';
			} else if ( 'v' == substr( $settings['apiVersion'], 0, 1 ) ) {
				$this->__apiVersion = $settings['apiVersion'];
			} else {
				$this->__apiVersion = 'v' . $settings['apiVersion'];
			}
		}
	}

	/**
	 * retrieve user/page feed
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/user/feed/
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/page/feed/
	 */
	public function feed( $user, $params = array(), $function = 'feed' ) {
		$requiredParams = array();
		$optionalParams = array(
			'limit',
			'fields',
			'include_hidden', // (bool)
			'include_inline', // (bool) promotable_posts only
			'is_published',   // (bool) promotable_posts only
		);

		$deep = isset($params['deep']) ? $params['deep'] : false;
		if ( $deep ) {
			if ( !isset($params['fields']) ) {
				$params['fields'] = array();
			}
			if ( is_array($params['fields']) ) {
				$params['fields'] += array('type', 'link');
			} else {
				$params['fields'] .= 'type,link';
			}
		}

		$params = $this->_parseParams($params, $requiredParams, $optionalParams);

		if( $params === false ) {
			return false;
		} else {
			$output = $this->_getData( $user, $function, $params );
			if( isset($output['data']) && count($output['data']) > 0 ) {
				if( $deep ) {
					$postParams = array(
						'fields' => isset($params['fields']) ? $params['fields'] : null,
					);
					for( $i = 0, $count = count($output['data']); $i < $count; $i++ ) {
						$output['data'][$i] = $this->deep($output['data'][$i], $postParams);
					}
				}

				return $output['data'];
			} else {
				return false;
			}
		}
	}

	/**
	 * retrieve user/page posts
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/user/feed/
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/page/feed/
	 */
	public function posts( $user, $params = array() ) {
		return $this->feed( $user, $params, 'posts' );
	}

	public function post( $id, $params = array() ) {
		$requiredParams = array();
		$optionalParams = array(
			'fields',
		);
		$params = $this->_parseParams($params, $requiredParams, $optionalParams);

		return $this->_getData( $id, '', $params );
	}

	/**
	 * retrieve user statuses
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/user/feed/
	 */
	public function statuses( $user, $params = array() ) {
		$function = 'statuses';

		$requiredParams = array();
		$optionalParams = array(
			'limit',
			'with',		// Restrict the list of posts to only those with location attached.
			'locale',
			'filter',	// Retrieve only posts that match a particular stream filter.
		);
		$params = $this->_parseParams($params, $requiredParams, $optionalParams);

		if( $params === false ) {
			return false;
		} else {
			$output = $this->_getData( $user, $function, $params );
			if( isset($output['data']) && count($output['data']) > 0 ) {
				return $output['data'];
			} else {
				return false;
			}
		}
	}

/**
 * Get an element
 */
	function get( $id, $params = array() ) {
		$optionalParams = array(
			'fields',
			'locale',
		);
		$params = $this->_parseParams($params, array(), $optionalParams);

		if( $params === false ) {
			return false;
		} else {
			$output = $this->_getData( $id, '', $params );
			if( isset($output['error']) ) {
				return false;
			} else {
				return $output;
			}
		}
	}

/**
 * Deep query, eg. for link post types
 */
	public function deep( $item, $params = array() ) {
		if( isset($item['type']) ) {
			if ( in_array($item['type'], array('photo', 'video')) && isset($item['object_id']) ) {
				$item['_images'] = Hash::extract($this->_getData($item['object_id'], '', array('fields' => 'images')), 'images');
			}
			if (
				'link' == $item['type']
				&& isset($item['link'])
				&& preg_match('#^https?://\w+\.facebook\.com/(.*)$#', $item['link'], $href)
			) {
				$href = Hash::filter(explode('/', current(explode('?', $href[1]))));
				$type = 'user';
				$id = $href[0];
				if( count($href) > 1 ) {
					switch( $href[0] ) {
					case 'events':
						$type = 'event';
						$id = $href[1]; // fb.com/events/{event_id}
						break;
					case 'pages':
						$type = 'page';
						$id = $href[2]; // fb.com/pages/{page_name}/{page_id}
						break;
					}
				}
				$item['_link_details'] = (array) $this->get( $id, $params ) + array('_node_type' => $type );
			}
		}
		return $item;
	}

	protected function _parseParams($params, $requiredParams, $optionalParams) {
		$newParams = array();
		foreach( $requiredParams as $p ) {
			if( array_key_exists($p, $params)) {
				$newParams[$p] = $params[$p];
			} else {
				return false;
			}
		}
		foreach( $optionalParams as $p ) {
			if( array_key_exists($p, $params)) {
				$newParams[$p] = $params[$p];
			}
		}

		return $newParams;
	}

	protected function _getData( $user, $function = '', $params = array() ) {
		$url = $this->_createUrl( $user, $function );
		$cachePath = 'facebook_graph_component_' . sha1($url.serialize($params));
		if( ($results = Cache::read($cachePath, $this->settings['cache_config'])) === false || $this->resetCache ) {
			// fields parameter is a string
			if ( isset($params['fields']) && is_array($params['fields']) ) {
				$params['fields'] = implode(',', $params['fields']);
			}

			// manually build uri because HttpSocket will urlencode the | (pipe)
			$request = $this->_createClient()->get(
				$url . '?'. http_build_query($params, '', '&') . sprintf('&access_token=%s|%s', $this->settings['appId'], $this->settings['appSecret'])
			);
			if(is_string($request->body)) {
				$results = $request->body;
				Cache::write($cachePath, $results, $this->settings['cache_config']);
			} else {
				$results = "";
			}
		}

		$output = false;
		$json = json_decode( $results, true );

		if( $json !== NULL ) {
			$output = $json;
		}

		if( $output === false ) {
			Cache::delete($cachePath, $this->settings['cache_config']);
		}
		return $output;
	}

	protected function _createUrl( $user, $function ) {
		return $this->__apiUrl . '/'. (!empty($this->__apiVersion) ? $this->__apiVersion . '/' : '') . urlencode($user) . '/' . $function;
	}

	protected function _createClient() {
		if( !isset( $this->_client ) ) {
			$this->_client = new HttpSocket();
		}
		return $this->_client;
	}
}
