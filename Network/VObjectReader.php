<?php
/**
 * Feed Reader
 *
 * Retrieve & parse rss/atom feeds with caching
 */
App::uses('HttpSocket', 'Network/Http');
use Sabre\VObject;

class VObjectReader {
/**
 * Settings for this class
 *
 * @var array
 */
	public $settings = array();
	protected $vObject;
	protected $vtimezones = array();

	const VOBJECT = 'VOBJECT'; // just the Sabre\VObject\Document
	const ARRAY_O = 'ARRAY_O'; // array of (normal) objects
	const ARRAY_A = 'ARRAY_A'; // array of named arrays (this is default)

	var $_cacheSettings;
	var $resetCache = false;


	public function __construct($settings = array()) {
		$this->settings = Hash::merge(array('cache_config' => 'default', 'output' => self::ARRAY_A, 'removeCustomFields' => false), $settings);
	}

	public function readUrl($url, $settings = array(), $socketOptions = array()) {
		$cachePath = 'dkzrutils_network_vobject_' . sha1($url);
		if( ($results = Cache::read($cachePath, $this->settings['cache_config'])) === false || $this->resetCache ) {
			$HttpSocket = new HttpSocket( $socketOptions );
			$request = false;

			try {
				$request = $HttpSocket->get($url);
			} catch( Exception $e ) {
				$request = false;
			}

			if( $request && is_string($request->body) ) {
				$results = $request->body;
				Cache::write($cachePath, $results, $this->settings['cache_config']);
			} else {
				return false;
			}
		}

		return $this->read($results, $settings);
	}

	public function read($content, $settings = array()) {
		$settings = Hash::merge($this->settings, $settings);

		$this->vtimezones = array();
		$this->vObject = \Sabre\VObject\Reader::read($content);

		if ($settings['removeCustomFields']) {
			$this->removeCustomFields();
		}

		switch($this->vObject->getDocumentType()) {
			case \Sabre\VObject\Document::ICALENDAR20:
				$output = $this->_ICALENDAR20($settings);
				break;
			case \Sabre\VObject\Document::VCARD21:
				$output = $this->_VCARD21($settings);
				break;
			case \Sabre\VObject\Document::VCARD30:
				$output = $this->_VCARD30($settings);
				break;
			case \Sabre\VObject\Document::VCARD40:
				$output = $this->_VCARD40($settings);
				break;
			default:
				$output = array();
		}

		return $output;
	}

/**
 * Expands elements when 'start' and 'end' filters are supplied
 * Splits the calendar into separate items
 */
	protected function _ICALENDAR20($settings = array()) {
		if (isset($settings['filter']['start']) && isset($settings['filter']['end'])) {
			$this->vObject->expand(date_create($settings['filter']['start']), date_create($settings['filter']['end']));
		}

		if (self::VOBJECT == $settings['output']) {
			return $this->vObject;
		}

		$objects = array();
		foreach ($this->vObject->children() as $component) {
			if ((!$component instanceof \Sabre\VObject\Component)) {
				continue;
			}

			// Get all timezones
			if ($component->name === 'VTIMEZONE') {
				$this->vtimezones[(string)$component->TZID] = $component;
				continue;
			}

			$objects[] = $component;
		}

		$output = array();
		foreach ($objects as $object) {
			$output[(string) $object->UID] = $this->simplefy($object, $settings);
		}

		return $output;
	}

/**
 * For vCards, only the first card is returned
 */
	protected function _VCARD21($settings = array()) {
		if (self::VOBJECT == $this->settings['output']) {
			return $this->vObject;
		} else {
			return $this->simplefy($settings);
		}
	}

	protected function _VCARD30($settings = array()) {
		return $this->_VCARD21($settings);
	}

	protected function _VCARD40($settings = array()) {
		return $this->_VCARD21($settings);
	}

	public function removeCustomFields($object = null) {
		if (!is_object($object)) {
			$object = $this->vObject;
		}

		foreach ($object->children() as $child) {
			if ($child instanceof \Sabre\VObject\Component) {
				$child = $this->removeCustomFields($child);
			} else if ($child instanceof \Sabre\VObject\Property && 'X-' == substr($child->name, 0, 2)) {
				$object->remove($child->name);
			}
		}
	}

	public function simplefy($object = null, $settings = array()) {
		if (!is_object($object)) {
			$settings = (array) $object;
			$object = $this->vObject;
		}

		$format = $this->settings['output'];
		if (isset($settings['output'])) {
			$format = $settings['output'];
		}
		if (self::ARRAY_O == $format) {
			$output = new stdClass();
		} else {
			$output = array();
		}

		foreach($object->children() as $child) {
			if ($child instanceof \Sabre\VObject\Property) {
				switch($child->getValueType()) {
					case 'BINARY':
						$value = $child->getRawMimeDirValue();
						break;
					case 'BOOLEAN':
						$value = (bool) $child->getValue();
						break;
					case 'INTEGER':
						$value = (int) $child->getValue();
						break;
					case 'FLOAT':
						$value = (float) $child->getValue();
						break;



					/**
					 * The vObject date/time strings are 20141120 or 20141120T1201Z which
					 * are easy to read and easy to convert using `strtotime()`, so we just
					 * use the value.
					 */
					case 'TIME':
						//$timeParts = \Sabre\VObject\DateTimeParser::parseVCardTime($child->getValue());
						//$value = "$timeParts[hour]:$timeParts[minute]:$timeParts[second] $timeParts[timezone]";
						//break;

						// Fallthrough !
					case 'TIMESTAMP':
					case 'DATE-AND-OR-TIME':
					case 'DATE-TIME':
					case 'DATE':
						//$dateParts = \Sabre\VObject\DateTimeParser::parseVCardDateTime($child->getValue());
						//$value = "$dateParts[year]-$dateParts[month]-$dateParts[date] $dateParts[hour]:$dateParts[minute]:$dateParts[second] $dateParts[timezone]";

						$value = $child->getValue();
						break;



					case 'TEXT':
					case 'CAL-ADDRESS':
						$value = trim($child->getValue());
						break;
					case 'RECUR':    // ???
					case 'PERIOD':   // ???
					case 'DURATION': // ???
					case 'UNKNOWN':
					case 'LANGUAGE-TAG':
					case 'UTC-OFFSET':
					case 'URI':
					default:
						$value = $child->getValue();
				}

				if (is_object($output)) {
					$output->{$child->name} = $value;
				} else {
					$output[$child->name] = $value;
				}
			}
		}

		if (\Sabre\VObject\Document::ICALENDAR20 == $this->vObject->getDocumentType()) {
			$vCalendar = new \Sabre\VObject\Component\VCalendar();
			$vCalendar->add(clone $object);
			$vCalendar->version = '2.0';
			$vCalendar->prodid = '-//Sabre//Sabre VObject ' . \Sabre\VObject\Version::VERSION . '//EN';
			$vCalendar->calscale = 'GREGORIAN';

			// add vtimezone information to vCalendar (if we have it)
			foreach ($this->vtimezones as $vtimezone) {
				$vCalendar->add($vtimezone);
			}

			$serialized = $vCalendar->serialize();
		} else {
			$serialized = $object->serialize();
		}
		if (is_object($output)) {
			$output->_serialized = $serialized;
		} else {
			$output['_serialized'] = $serialized;
		}

		return $output;
	}
}
