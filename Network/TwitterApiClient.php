<?php
/**
 * Twitter Api Client
 * 
 * Timeline resources
 * - http://dev.twitter.com/doc/get/statuses/public_timeline (json, xml, rss, atom)
 * - http://dev.twitter.com/doc/get/statuses/user_timeline (json, xml, rss, atom)
 * 
 * Tweets resources
 * - http://dev.twitter.com/doc/get/statuses/show/:id (xml, json)
 * - http://dev.twitter.com/doc/get/statuses/retweets/:id (xml, json)
 * 
 * User resources
 * - http://dev.twitter.com/doc/get/users/show (xml, json)
 * - http://dev.twitter.com/doc/get/users/suggestions (xml, json)
 * - http://dev.twitter.com/doc/get/users/profile_image/:screen_name (xml, json)
 * - http://dev.twitter.com/doc/get/statuses/friends (xml, json)
 * - http://dev.twitter.com/doc/get/statuses/followers (xml, json)
 * 
 * Trends resources / Local Trends resources
 * - http://dev.twitter.com/doc/get/trends (json)
 */
App::uses('HttpSocket', 'Network/Http');

class TwitterApiClient {
/**
 * Settings for this class
 *
 * @var array
 */
	public $settings = array();

	protected $__apiUrl = "https://api.twitter.com";
	protected $__apiVersion = "1.1";

	protected $__twitterUrl = "http://twitter.com";

	protected $_client;

	// json is preferred & default format for now
	protected $__format = "json";

	protected $_cacheSettings;

	public $resetCache = false;

	public function __construct($settings = array()) {
		$this->settings = Hash::merge(array('cache_config' => 'default'), $settings);
	}

	/**
	 * global format options are:
	 * - json (default)
	 * - xml
	 * - rss
	 * - atom
	 */
	public function setFormat ($f) {
		if( in_array( $f, array('json','xml','rss','atom') ) ) {
			$this->__format = $f;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * retrieve userTimeline
	 * @params: see https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
	 */
	function userTimeline( $params ) {
		$function = "statuses/user_timeline";

		// preferred format is first item
		$allowedFormats = array('json', 'xml', 'rss', 'atom');

		$requiredParams = array();
		$optionalParams = array(
			'user_id',				// id (number)
			'screen_name',			// string
			'since_id',				// id (number)
			'count',				// number <= 200
			'max_id',				// id (number)
			'trim_user',			// bool (true, t or 1)
			'exclude_replies',		// bool (true, t or 1)
			'contributor_details',	// bool (true, t or 1)
			'include_rts',			// bool (true, t or 1)
		);
		$params = $this->_parseParams($params, $requiredParams, $optionalParams);

		if( $params === false ) {
			return false;
		} else {
			if( $output = $this->_formatTweets($this->_getData($function, $params, $allowedFormats), $params) ) {
				return $output;
			} else {
				return false;
			}
		}
	}

	protected function _formatTweets( $results = array() ) {
		if( !is_array($results) || count($results) == 0 || isset($results['error']) ) return false;
		foreach( $results as $k => $tweet ) {
			if( isset($tweet['text']) && isset($tweet['entities']) ) {
				$indices = array();
				foreach( $tweet['entities'] as $type => $entities ) {
					foreach( $entities as $item ) {
						$mod = array(
							'from' => $item['indices'][0],
							'to'   => $item['indices'][1],
						);
						switch( $type ) {
						case "user_mentions":
							$mod['expected'] = sprintf('@%s', $item['screen_name']);
							$mod['replace']  = sprintf('<a href="https://twitter.com/%1$s" class="mention" rel="tag" data-scribe="element:mention"><s>@</s><b>%1$s</b></a>', $item['screen_name']);
							break;
						case "hashtags":
							$mod['expected'] = sprintf('#%s', $item['text']);
							$mod['replace']  = sprintf('<a href="https://twitter.com/search?q=%%23%1$s&src=hash" class="hashtag" rel="tag" data-scribe="element:hashtag"><s>#</s><b>%1$s</b></a>', $item['text']);
							break;
						case "symbols":
							$mod['expected'] = sprintf('$%s', $item['text']);
							$mod['replace']  = sprintf('<a href="https://twitter.com/search?q=%%24%1$s&src=ctag" class="ctag" rel="tag" data-scribe="element:ctag"><s>$</s><b>%1$s</b></a>', $item['text']);
							break;
						case "media":
							if (!isset($results[$k]['media'])) {
								$results[$k]['media'] = array();
							}
							$results[$k]['media'][] = array(
								'id'              => $item['id'],
								'type'            => $item['type'],
								'media_url'       => $item['media_url'],
								'media_url_https' => $item['media_url_https'],
								'url'             => $item['url'],
								'display_url'     => $item['display_url'],
								'expanded_url'    => $item['expanded_url'],
								'sizes'           => $item['sizes'],
							);
							// fallthrough
						case "urls":
							$mod['expected'] = $item['url'];
							$mod['replace']  = sprintf('<a href="%s" class="url" rel="tag" data-scribe="element:url">%s</a>', (strlen($item['expanded_url']) > 0 ? $item['expanded_url'] : $item['url']), (strlen($item['display_url']) > 0 ? $item['display_url'] : $item['url']));
							break;
						}
						$mod['delta']          = strlen($mod['replace']) - strlen($mod['expected']);
						$indices[$mod['from']] = $mod;
					}
				}
				ksort($indices);
				$htmlText = $tweet['text'];
				$deltaSum = 0;
				foreach ($indices as $mod) {
					$htmlText = substr_replace($htmlText, $mod['replace'], $mod['from'] + $deltaSum, strlen($mod['expected']));
					$deltaSum += $mod['delta'];
				}

				$results[$k]['htmlText'] = "<p>" . $htmlText . "</p>";

				if( isset($tweet['user']['screen_name']) ) {
					$results[$k]['url'] = $this->__twitterUrl."/".$tweet['user']['screen_name']."/status/".$tweet['id_str'];
				}

				unset($results[$k]['entities']);
			}
		}
		return $results;
	}

	protected function _parseParams($params, $requiredParams, $optionalParams) {
		$newParams = array();
		foreach( $requiredParams as $p ) {
			if( array_key_exists($p, $params)) {
				$newParams[$p] = $params[$p];
			} else {
				return false;
			}
		}
		foreach( $optionalParams as $p ) {
			if( array_key_exists($p, $params)) {
				$newParams[$p] = $params[$p];
			}
		}

		return $newParams;
	}

	protected function _getData( $function, $params = array(), $allowedFormats = array() ) {
		$format = $this->__format;
		if( !in_array($format, $allowedFormats) ) {
			$format = reset($allowedFormats);
		}

		$url = $this->_createUrl( $function, $format );
		$cachePath = 'twitter_component_' . sha1($url.serialize($params));
		if( ($results = Cache::read($cachePath, $this->settings['cache_config'])) === false || $this->resetCache ) {
			$request = $this->_createClient()->get(
				$this->settings['accessToken'],
				$this->settings['accessTokenSecret'],
				$url,
				$params
			);
			if(is_string($request->body)) {
				$results = $request->body;
				Cache::write($cachePath, $results, $this->settings['cache_config']);
			} else {
				$results = "";
			}
		}
		$output = false;
		switch( $format ) {
		case "json":
			$json = json_decode( $results, true );
			if( $json !== NULL ) {
				$output = $json;
			}
			break;
		}

		if( $output === false ) {
			Cache::delete($cachePath, $this->settings['cache_config']);
		}
		return $output;
	}

	protected function _createUrl( $function, $format = "" ) {
		if( $format == "" ) {
			$format = $this->__format;
		}

		return $this->__apiUrl."/".$this->__apiVersion."/".$function.".".$format;
	}

	protected function _createClient() {
		if( !isset( $this->_client ) ) {
			if (!class_exists('OAuthClient')) {
				App::uses('OAuthClient', 'Vendor/OAuth');
			}
			$this->_client = new OAuthClient(
				$this->settings['consumerKey'],
				$this->settings['consumerSecret']
			);
		}
		return $this->_client;
	}
}
