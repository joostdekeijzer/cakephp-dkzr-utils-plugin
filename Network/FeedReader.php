<?php
/**
 * Feed Reader
 * 
 * Retrieve & parse rss/atom feeds with caching
 */
App::uses('HttpSocket', 'Network/Http');

class FeedReader {
/**
 * Settings for this class
 *
 * @var array
 */
	public $settings = array();

	var $_cacheSettings;
	var $resetCache = false;

	public function __construct($settings = array()) {
		$this->settings = Hash::merge(array('cache_config' => 'default'), $settings);
	}

	public function feed( $feed_url, $count = null, $socketOptions = array() ) {
		// Load RSS file
		if($rss = $this->_getData( $feed_url, $count, $socketOptions ) ) {
			$items = $rss;
			return $items;
		} else {
			// Return false
			return false;
		}
	}

	protected function _getData( $url, $count = null, $socketOptions = array() ) {
		$cachePath = 'feed_component_' . sha1($url);
		if( ($results = Cache::read($cachePath, $this->settings['cache_config'])) === false || $this->resetCache ) {
			$HttpSocket = new HttpSocket( $socketOptions );
			$request = false;

			try {
				$request = $HttpSocket->get($url);
			} catch( Exception $e ) {
				$request = false;
			}

			if( $request && is_string($request->body) ) {
				$results = $request->body;
			} else {
				$results = 'failed';
			}
			Cache::write($cachePath, $results, $this->settings['cache_config']);
		}

		if( 'failed' === $results ) {
			return false;
		}

		$output = false;
		if( $xml = simplexml_load_string($results, "SimpleXMLElement", LIBXML_NOERROR) ) { // creates SimpleXMLElement or bool false
			switch( $xml->getName() ) {
			case "rss":
				$format = "rss";
				$output = array();
				foreach( $xml->channel as $channel ) {
					$output[] = $this->_parseRss( $channel, $count );
				}
				if( count( $output ) == 1 ) $output = $output[0];
				break;
			case "feed":
				$format = "atom";
				$output = $this->_parseAtom( $xml, $count );
				break;
			}
		}

		if( $output === false ) {
			Cache::delete($cachePath, $this->settings['cache_config']);
		}
		return $output;
	}

	protected function _parseRss( $xml, $count = null ) {
		$output = array(
			'feed' => array(
				'title' => (string) $xml->title,
				'description' => (string) $xml->description,
				'author' => (string) $xml->managingEditor,
				'lastUpdate' => strtotime( (string) $xml->lastBuildDate ),
				'url' => (string) $xml->link,
				'feedType' => 'rss',
			),
			'items' => array(),
		);

		$i = 0;
		foreach( $xml->item as $item ) {
			$output['items'][] = array(
				'title' => (string) $item->title,
				'description' => (string) $item->description,
				'author' => (string) $item->author,
				'published' => strtotime( (string) $item->pubDate ),
				'url' => (string) $item->link,
			);
			$i++;
			if( is_numeric($count) && $i >= $count ) break;
		}

		return $output;
	}

	protected function _parseAtom( $xml, $count = null ) {
		$namespaces = $xml->getDocNamespaces();
		$xml->registerXPathNamespace('__', $namespaces['']);
		$output = array(
			'feed' => array(
				'title' => (string) $xml->title,
				'description' => (string) $xml->subtitle,
				'author' => (string) $xml->author->name,
				'lastUpdate' => strtotime( (string) $xml->updated ),
				'url' => (string) reset($xml->xpath('__:link[@rel="alternate"]/@href')),
				'feedType' => 'atom',
			),
			'items' => array(),
		);

		$i = 0;
		foreach( $xml->entry as $entry ) {
			$entry->registerXPathNamespace('__', $namespaces['']);
			$output['items'][] = array(
				'title' => (string) $entry->title,
				'description' => (string) $entry->content,
				'author' => (string) $entry->author->name,
				'published' => strtotime( (string) $entry->published ),
				'url' => (string) reset($entry->xpath('__:link[@rel="alternate"]/@href')),
			);
			$i++;
			if( is_numeric($count) && $i >= $count ) break;
		}

		return $output;
	}

}
