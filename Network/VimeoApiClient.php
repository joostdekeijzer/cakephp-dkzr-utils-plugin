<?php
/**
 * Vimeo Api Client
 */
App::uses('HttpSocket', 'Network/Http');

class VimeoApiClient {
/**
 * Settings for this class
 *
 * @var array
 */
	public $settings = array();

	protected $_client;

	protected $_cacheSettings;

	public $resetCache = false;

	public function __construct($settings = array()) {
		$this->settings = Hash::merge(array('cache_config' => 'default'), $settings);
	}

	public function getVideoThumbnails($videoId) {
		return $this->_getData( sprintf( '/videos/%d/pictures/', $videoId ) );
	}

	public function setFormat ($f) {
		return true;
	}

	protected function _getData( $url, $params = array(), $method = 'GET' ) {
		$cachePath = 'vimeo_component_' . sha1($method.$url.serialize($params));

		if( ($results = Cache::read($cachePath, $this->settings['cache_config'])) === false || $this->resetCache ) {
			$request = $this->_createClient()->request(
				$url,
				$params,
				$method
			);

			if( isset( $request['body'] ) && ! isset( $request['body']['error'] ) ) {
				$results = $request['body'];
				Cache::write($cachePath, $results, $this->settings['cache_config']);
			} else {
				$results = false;
			}
		}

		return $results;
	}

	protected function _createClient() {
		if( !isset( $this->_client ) ) {
			$this->_client = new \Vimeo\Vimeo(
				$this->settings['consumerKey'],
				$this->settings['consumerSecret']
			);
		}

		if ( ! empty($this->settings['accessToken']) ) {
			$this->_client->setToken($this->settings['accessToken']);
		}

		return $this->_client;
	}
}
