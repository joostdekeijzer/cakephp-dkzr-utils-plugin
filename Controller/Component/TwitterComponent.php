<?php
/**
 * Twitter Component
 * 
 * The class is now located in Network/, this is a legacy solution.
 */
App::uses('TwitterApiClient', 'DkzrUtils.Network');

class TwitterComponent extends Component {
	protected $_client;

	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_client = new TwitterApiClient($settings);
	}

	public function setFormat ($f) {
		return $this->_client->setFormat($f);
	}

	public function userTimeline( $params ) {
		return $this->_client->userTimeline($params);
	}
}
