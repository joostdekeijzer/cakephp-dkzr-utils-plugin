<?php 
/**
 * Retrieve & parse rss/atom feeds with caching
 * 
 * The class is now located in Network/, this is a legacy solution.
 */
App::uses('FeedReader', 'DkzrUtils.Network');

class FeedComponent extends Component {
	protected $_client;

	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_client = new FeedReader($settings);
	}

	public function feed( $feed_url, $count = null, $socketOptions = array() ) {
		return $this->_client->feed($feed_url, $count, $socketOptions);
	}
}
