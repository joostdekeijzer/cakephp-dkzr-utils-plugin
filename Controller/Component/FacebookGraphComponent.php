<?php
/**
 * Facebook Graph Component
 * 
 * The class is now located in Network/, this is a legacy solution.
 */
App::uses('FacebookGraphApiClient', 'DkzrUtils.Network');

class FacebookGraphComponent extends Component {
	protected $_client;
	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_client = new FacebookGraphApiClient($settings);
	}

	public function feed( $user, $params = array(), $function = 'feed' ) {
		return $this->_client->feed($user, $params, $function);
	}

	/**
	 * retrieve user/page posts
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/user/feed/
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/page/feed/
	 */
	public function posts( $user, $params = array() ) {
		return $this->feed( $user, $params, 'posts' );
	}

	/**
	 * retrieve user statuses
	 * @params: see https://developers.facebook.com/docs/graph-api/reference/user/feed/
	 */
	public function statuses( $user, $params = array() ) {
		return $this->_client->statuses($user, $params);
	}

/**
 * Get an element
 */
	public function get( $id, $params = array() ) {
		return $this->_client->get($user, $params);
	}

/**
 * Deep query, eg. for link post types
 */
	public function deep( $item ) {
		return $this->_client->deep($item);
	}
}
