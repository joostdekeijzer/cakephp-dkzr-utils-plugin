<?php
/**
 * Vimeo Component
 * 
 * The class is now located in Network/, this is a legacy solution.
 */
App::uses('VimeoApiClient', 'DkzrUtils.Network');

class VimeoComponent extends Component {
	protected $_client;

	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_client = new VimeoApiClient($settings);
	}

	public function getVideoThumbnails($videoId) {
		return $this->_client->getVideoThumbnails($videoId);
	}

	/**
	 * global format options are:
	 * - json (default)
	 * - jsonp
	 * - xml
	 * - php
	 */
	public function setFormat ($f) {
		return $this->_client->setFormat($f);
	}
}
