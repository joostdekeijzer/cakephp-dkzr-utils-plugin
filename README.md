CakePHP-DkzrUtils
=============================

CakePHP v2 plugin with some conveniance functions

In your `bootstrap.php`, include:
```
CakePlugin::load('DkzrUtils', array('bootstrap' => true));
```

Currently, there is one function in bootstrap which I like a lot:
`epr()` is the equivalent to CakePHP `pr()`, but outputs to the php error-log.

Also, `epr` accepts multiple arguments, just outputting them all, so eg. you can do:
```
class MyModel extends AppModel {
    beforeFilter(Model $Model) {
       epr(__method__, __line__, $Model->hasMany);
       // rest of code...
    }
}
```