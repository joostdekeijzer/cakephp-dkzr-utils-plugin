<?php
/**
 * Plugin Boostrap
 */

if (!function_exists('epr')) {

/**
 * Variant of pr() which outputs to the error_log
 */
	function epr() {
		if(Configure::read('debug') > 0) {
			if( func_num_args() > 1 ) {
				$input = func_get_args();
			} else {
				$input = func_get_arg(0);
			}
			error_log( print_r( $input, true ) );
		}
	}

}
