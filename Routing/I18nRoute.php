<?php
/**
 * SetLocale helper
 */
App::uses('I18n', 'I18n');
App::uses('CakeRoute', 'Routing/Route');

class I18nRoute extends CakeRoute {
	public $rawTemplate = null;
	public $currentLanguage = null;
	public $i18nTemplates = array();
	public $i18nRoutes = array();

	public $redirect = null;

	public function __construct($rawTemplate, $defaults = array(), $options = array()) {
		$this->rawTemplate = (array) $rawTemplate;
		$template = $this->_writeTemplate();

		if (isset($options['redirect']) && $options['redirect']) {
			$this->redirect = compact('defaults', 'options');
		}

		return parent::__construct($template, $defaults, $options);
	}

	public function compiled($language = null) {
		if (empty($language)) {
			$language = $this->currentLanguage;
		}
		if ($language != $this->currentLanguage) {
			$this->_writeTemplate($language);
			return false;
		} else {
			return parent::compiled();
		}
	}

	public function match($url) {
		if (null !== $this->redirect) return false;

		if (isset($url['language'])) {
			$this->compiled($url['language']);
			unset($url['language']);
		} else {
			$this->compiled(Configure::read('Config.language'));
		}

		return parent::match($url);
	}

	public function compile() {
		$this->compiled();
		return parent::compile();
	}

	public function parse($url) {
		$this->compiled(Configure::read('Config.language'));

		if (null !== $this->redirect) {
			App::uses('RedirectRoute', 'Routing/Route');
			$redirectRoute = new RedirectRoute($this->template, $this->redirect['defaults'], $this->redirect['options']);
			return $redirectRoute->parse($url);
		} else {
			return parent::parse($url);
		}
	}

	protected function _writeTemplate($language = null) {
		if (empty($language)) {
			$this->currentLanguage = Configure::read('Config.language');
		} else {
			$this->currentLanguage = $language;
		}

		if (!array_key_exists($this->currentLanguage, $this->i18nTemplates)) {
			/**
			 * The L10n default (set when I18n or L10n is first called) interferes
			 * with the dynamic changing of a language, so we set it to null first
			 */
			$I18n = I18n::getInstance();
			$l10nDefault = $I18n->l10n->default;
			$I18n->l10n->default = null;

			$template = $I18n->translate($this->rawTemplate[0], null, null, I18n::LC_MESSAGES, null, $this->currentLanguage);

			$this->i18nTemplates[$this->currentLanguage] = vsprintf($template, array_slice($this->rawTemplate, 1));

			$I18n->l10n->default = $l10nDefault;
		}

		$this->_compiledRoute = null; // so parent::compiled() will return false
		$this->template = $this->i18nTemplates[$this->currentLanguage];

		return $this->template;
	}

/**
 * Detect current language from url.
 *
 * Attach to Dispatcher.beforeDispatch event in bootstrap.php after
 * Config.laguage and Config.locales configurations have been set:
 *
 * Example:
 *
 * {{{
 * Configure::write('Config.language', 'nld');
 * Configure::write('Config.locales', array(
 * 	'nld' => array(
 * 		'urlPrefix' => 'nl',
 * 		'locale'    => 'nl_NL.UTF-8',
 * 	),
 * 	'eng' => array(
 * 		'urlPrefix' => 'en',
 * 		'locale'    => 'en_GB.UTF-8',
 * 	),
 * 	'pt_br' => array(
 * 		'urlPrefix' => 'br',
 * 		'language'  => 'pt-br', // pt_br/pt-br language/locale mixup in CakePHP
 * 		'locale'    => 'pt_br.UTF-8',
 * 	),
 * ));
 *
 * App::uses('CakeEventManager', 'Event');
 * App::uses('I18nRoute', 'DkzrUtils.Routing');
 * CakeEventManager::instance()->attach(array('I18nRoute', 'detectLanguage'), 'Dispatcher.beforeDispatch');
 * }}}
 *
 * When `Configure::read('I18nRoute.foundLanguage')` (bool) is available, it will be set (and there won't be an error)
 *
 * @param CakeEvent $event
 * @return void
 * @throws NotFoundException
 */
	public static function detectLanguage(CakeEvent $event) {
		$url = $event->data['request']->url;
		$needle = Hash::get(Hash::filter(explode('/', $url)), '0');

		$foundLanguage = false;
		$fallbackLanguage = false;

		if (in_array($needle, Configure::read('Routing.prefixes'))) {
			// we ignore the Routing.prefixes
			$foundLanguage = Configure::read('Config.language');
		} else {
			// loop through Config.locales to find the urlPrefix
			foreach (Configure::read('Config.locales') as $locale => $settings) {
				if (strlen($settings['urlPrefix']) > 0) {
					if ($needle == $settings['urlPrefix']) {
						$foundLanguage = (isset($settings['language']) ? $settings['language'] : $locale);
						break;
					}
				} else {
					// empty string urlPrefix makes it fallbackLanguage
					$fallbackLanguage = (isset($settings['language']) ? $settings['language'] : $locale);
				}
			}
		}

		if (!$foundLanguage && $fallbackLanguage) {
			$foundLanguage = $fallbackLanguage;
		}

		if ($foundLanguage) {
			if (!is_null(Configure::read('I18nRoute.foundLanguage'))) {
				Configure::write('I18nRoute.foundLanguage', true);
			}

			Configure::write('Config.language', $foundLanguage);

			$I18n = I18n::getInstance();
			$I18n->l10n->get(Configure::read('Config.language'));
			setlocale(LC_ALL, Hash::get(Configure::read('Config.locales'), $I18n->l10n->locale . '.locale'));
		} else {
			// urlPrefix not found and no fallbackLanguage (with empty urlPrefix)
			if (!is_null(Configure::read('I18nRoute.foundLanguage'))) {
				Configure::write('I18nRoute.foundLanguage', false);
			} else {
				throw new NotFoundException();
			}
		}
	}
}
