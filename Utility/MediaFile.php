<?php
/**
 * MediaFile class
 *
 * Extends Utility/File
 */

App::uses('File', 'Utility');

class MediaFile extends File {
/**
 *
 */
	public function info() {
		parent::info();

		if(
			!isset($this->info['image'])
			&& $this->exists()
			&& ( false !== strstr($this->info['mime'], 'image') || in_array( strtolower($this->info['extension']), array('jpg', 'jpeg', 'png', 'gif') ) )
		) {
			$image = getimagesize($this->pwd());
			$image['width']  = array_shift($image);
			$image['height'] = array_shift($image);
			$image['type']   = array_shift($image);
			$image['attr']   = array_shift($image);

			$this->info['image'] = $image;
		}

		return $this->info;
	}

/**
 * $options['scale']         = float scale to resize to
 * $options['width']         = when scale is not set, exact pixel width
 * $options['height']        = when scale is not set, exact pixel height
 * $options['scaleMode']     = contain|cover (default: contain; verder zie css beschrijving)
 * $options['crop']          = when true and width/height are set, crop image to exact width/height after scaling (default: false)
 * $options['enlarge']       = when false, prevent scaling and copy (default: true)
 * $options['skipOnEnlarge'] = when true, prevent scaling and skip further processing (default: false)
 */
	public function resize($dest, $options, $overwrite = true) {
		if (!$this->exists() || is_file($dest) && !$overwrite) {
			return false;
		}

		$info = $this->info();
		if (!isset($info['image']['type']) || !in_array($info['image']['type'], array(IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG))) {
			return false;
		}

		$scale = 1;
		if( isset( $options['scale']) ) {
			$scale = $options['scale'];
		} elseif (isset($options['width']) || isset($options['height'])) {
			$scales = array();
			if (isset($options['width'])) {
				$scales['x'] = $options['width']/$info['image']['width'];
			}

			if (isset($options['height'])) {
				$scales['y'] = $options['height']/$info['image']['height'];
			}

			if ( isset($options['scaleMode']) && 'cover' ==  $options['scaleMode']) {
				$scale = max($scales);
			} else {
				$scale = min($scales);
			}
		}

		if (1 < $scale && isset($options['skipOnEnlarge']) && $options['skipOnEnlarge']) {
			return $dest;
		}

		if (1 < $scale && isset($options['enlarge']) && !$options['enlarge']) {
			$scale = 1;
		}

		// rewrite $dest to have correct extension
		$destFile = new File($dest);
		$dest = $destFile->folder()->slashTerm($destFile->folder()->pwd()) . $destFile->name();

		// When basename contains dots (.), `File` will interpret last part as the
		// extension (which it isn't), so append it.
		if ($destFile->ext()) {
			$dest .= '.' . $destFile->ext();
		}

		if ($this->ext()) {
			$dest .= '.' . $this->ext();
		}

		if (1 === $scale) {
			// copy
			$this->copy($dest, $overwrite);
		} elseif(extension_loaded('gmagick')) {
			// use GraphicsMagick
			$this->gmResize($this->pwd(), $dest, $scale * $info['image']['width'], $scale * $info['image']['height'], $options);
		} elseif(extension_loaded('imagick')) {
			// use ImageMagic
			$this->imResize($this->pwd(), $dest, $scale * $info['image']['width'], $scale * $info['image']['height'], $options);
		} else {
			// use GD
			$this->gdResize($info['image']['type'], $dest, $this->pwd(), $scale * $info['image']['width'], $scale * $info['image']['height'], $info['image']['width'], $info['image']['height'], $options);
		}

		if (isset($options['mode'])) {
			$this->folder()->chmod($dest, $options['mode']);
		}

		return $dest;
	}

	protected function gmResize($orig, $dest, $width, $height, $options = array(), $filter = Gmagick::FILTER_LANCZOS, $blur = 1) {
		$image = new Gmagick($orig);
		$image->resizeImage($width, $height, $filter, $blur);
		//$image->scaleImage($width, $height);

		if ( isset( $options['crop'], $options['width'], $options['height'] ) && $options['crop'] ) {
			$cWidth  = min( $width, $options['width'] );
			$cHeight = min( $height, $options['height'] );
			$cX = ( $width - $cWidth ) / 2;
			$cY = ( $height - $cHeight ) / 2;
			$image->cropImage( $cWidth, $cHeight, $cX, $cY );
		}

		$image->writeImage($dest);
		$image->destroy();
	}

	protected function imResize($orig, $dest, $width, $height, $options = array(), $filter = imagick::FILTER_LANCZOS, $blur = 1) {
		$image = new Imagick($orig);
		$image->resizeImage($width, $height, $filter, $blur);
		//$image->scaleImage($width, $height);

		if ( isset( $options['crop'], $options['width'], $options['height'] ) && $options['crop'] ) {
			$cWidth  = min( $width, $options['width'] );
			$cHeight = min( $height, $options['height'] );
			$cX = ( $width - $cWidth ) / 2;
			$cY = ( $height - $cHeight ) / 2;
			$image->cropImage( $cWidth, $cHeight, $cX, $cY );
		}

		//$image->normalizeImage();
		//$image->unsharpMaskImage(0, 0.5, 1, 0.05);
		$image->unsharpMaskImage(1, 0.5, 0.5, 0);

		//$image->setImageCompressionQuality(98);
		$image->writeImage($dest);
		$image->destroy();
	}

	protected function gdResize($type, $dest, $orig, $dest_width, $dest_height, $orig_width, $orig_height, $options = array(), $jpeg_quality = 85) {
		switch($type) {
		case IMAGETYPE_JPEG:
			$image = imagecreatefromjpeg($orig);
			break;
		case IMAGETYPE_GIF:
			$image = imagecreatefromgif($orig);
			break;
		case IMAGETYPE_PNG:
			$image = imagecreatefrompng($orig);
			break;
		default:
			return false; // just in case...
		}

		$newImage = imagecreatetruecolor($dest_width, $dest_height);
		imagecopyresampled(
			$newImage,
			$image,
			0, // x dest
			0, // y dest
			0, // x orig
			0, // y orig
			$dest_width, // width dest
			$dest_height, // height dest
			$orig_width, // width orig
			$orig_height // height orig
		);

		if ( isset( $options['crop'], $options['width'], $options['height'] ) && $options['crop'] ) {
			$cWidth  = min( $dest_width, $options['width'] );
			$cHeight = min( $dest_height, $options['height'] );
			$cX = ( $dest_width - $cWidth ) / 2;
			$cY = ( $dest_height - $cHeight ) / 2;
			$newImage = imagecrop( $newImage, array( 'x' => $cX, 'y' => $cY, 'width' => $cWidth, 'height' => $cHeight ) );
		}

		switch($type) {
		case IMAGETYPE_JPEG:
			imagejpeg($newImage, $dest, $jpeg_quality);
			break;
		case IMAGETYPE_GIF:
			imagegif($newImage, $dest);
			break;
		case IMAGETYPE_PNG:
			imagepng($newImage, $dest);
			break;
		}
	}
}
