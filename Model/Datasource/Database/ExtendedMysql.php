<?php
App::uses('Mysql', 'Model/Datasource/Database');

class ExtendedMysql extends Mysql {
	public function __construct($config = null, $autoConnect = true) {
		parent::__construct($config, $autoConnect);

		$this->columns['enum'] = array('name' => 'enum', 'options' => '');
	}

	public function describe($model) {
		$fields = parent::describe($model);

		if ( version_compare( Configure::version(), '2.10.14', '<' ) ) {
			foreach ($fields as $name => &$value) {
				if (isset($value['type']) && 'enum(' == substr($value['type'], 0, 5)) {
					$value['options'] = substr($value['type'], 5, -1);
					$value['type'] = 'enum';
				}
			}
		}

		return $fields;
	}

	public function buildColumn($column) {
		if (isset($column['type'], $column['options']) && 'enum' == $column['type']) {
			if ( version_compare( Configure::version(), '2.10.14', '>=' ) ) {
				$column['type'] = sprintf( '%s (%s)', $column['type'], $column['options'] );
			} else {
				$column['length'] = $column['options'];
			}
		}

		return parent::buildColumn($column);
	}
}
