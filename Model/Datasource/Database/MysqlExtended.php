<?php
App::uses('ExtendedMysql', 'DkzrUtils.Model/Datasource/Database');

/**
 * @deprecated deprecated in favor of DkzrUtils.Database/ExtendedMysql
 */
class MysqlExtended extends ExtendedMysql {}
