<?php
/**
 * UUIDv4 Id behavior class.
 *
 * The default CakePHP String::uuid() method returns a 'time based' UUID value.
 * Problem with this UUID is that the left-most characters are not very
 * significant. Usually no issue, but when you want to spread locations
 * based on the ID in a /rootdir/aa/bb/aabbxyz-etc/ maner, the UUIDv4 is better.
 *
 * When this behavior is loaded without setting priority (or setting it to the
 * defaultPriority (currently 10), Uuid4Id behavior will set it's priority to
 * 0 (zero) on setup.
 *
 * PHP 5
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Joost de Keijzer et. al. (http://dekeijzer.org)
 * @link          http://dekeijzer.org Joost de Keijzer
 * @package       DkrzUtils.Behavior
 * @since         4-may-2013
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('ModelBehavior', 'Model');

class Uuid4IdBehavior extends ModelBehavior {

	public function setup(Model $model, $config = array()) {
		// put this behaviours priority first in line when priority is defaultPriority
		if( $this->settings['priority'] == $model->Behaviors->defaultPriority ) {
			$model->Behaviors->setPriority( 'Uuid4Id', 0 );
		}
	}

	public function beforeSave(Model $model, $options = array()) {
		$alias = $model->alias;
		$primaryKey = $model->primaryKey;

		// UUID test copied from Model::_isUUIDField
		$field = $model->schema($primaryKey);
		if( $field['length'] == 36 && in_array($field['type'], array('string', 'binary')) ) {
			// sort-of copied from Model::save
			if( empty( $model->id ) && empty( $model->data[$alias][$primaryKey] ) ) {
				$model->data[$alias][$primaryKey] = Uuid4IdBehavior::uuid4();
			}
		}

		return true;
	}

	// public function setup(Model $model, $config = array()) {}
	// public function cleanup(Model $model) {}
	// public function afterSave(Model $model, $created) { return true;}
	// public function beforeFind(Model $model, $query) { return true; }
	// public function afterFind(Model $model, $results, $primary) {}
	// public function beforeValidate(Model $model) {}
	// public function afterValidate(Model $model) { return true; }
	// public function beforeDelete(Model $model, $cascade = true) {}
	// public function afterDelete(Model $model) {}

/****************************************************************************/
/* MARK: Static public functions                                            */
/****************************************************************************/

/**
 * (Pseudo) random uuid version 4 generator
 *
 * http://www.php.net/manual/en/function.uniqid.php#94959
 */
	public static function uuid4() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}
}
