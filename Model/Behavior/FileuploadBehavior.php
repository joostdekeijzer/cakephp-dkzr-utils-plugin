<?php
/**
 * Fileupload behavior class.
 *
 * Handles file uploads for a model.
 *
 * PHP 5
 *
 * CakePHP :  Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Joost de Keijzer et. al. (http://dekeijzer.org)
 * @link          http://dekeijzer.org Joost de Keijzer
 * @package       DkrzUtils.Behavior
 * @since         4-may-2013
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('ModelBehavior', 'Model');
App::uses('Set', 'Utility');

class FileuploadBehavior extends ModelBehavior {
/**
 * All "file" fields per model with settings
 */
	protected $_fileFields = array();

/**
 * Stack of file-uploads to be finished on afterSave
 */
	protected $_uploadStack = array();

/**
 * Descriptive texts for file upload errors
 *
 * @link http://php.net/manual/en/features.file-upload.errors.php
 */
	protected $_uploadErrorTexts = array(
		UPLOAD_ERR_OK         => 'The file was uploaded with success.',
		UPLOAD_ERR_INI_SIZE   => 'The uploaded file exceeds the `upload max filesize` PHP directive.',
		UPLOAD_ERR_FORM_SIZE  => 'The uploaded file exceeds the `MAX_FILE_SIZE` directive that was specified in the HTML form.',
		UPLOAD_ERR_PARTIAL    => 'The uploaded file was only partially uploaded.',
		UPLOAD_ERR_NO_FILE    => 'No file was uploaded.',
		UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
		UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
		UPLOAD_ERR_EXTENSION  => 'A PHP extension stopped the file upload.',
		'unknown'             => 'Unknown file upload error.'
	);

// MARK: Cake callbacks

/**
 * Callback
 *
 * $config for FileuploadBehavior should be
 * array('settings' => array(), 'fields' => array('field_one',
 * 'field_two' => array(), 'field_three'))
 *
 * The available settings are:
 * - uploadBaseDir: (string) path to where the images should be saved.
 *   DEFAULTS TO: IMAGES constant
 * - uploadDir: (string) subdirectory of uploadBaseDir. DEFAULTS TO: Model name
 * - useFieldname: (boolean|string) replace the files current filename to the fieldname.
 *   Possible values:
 *   - 'filename': the filename is replaced with the field_name.
 *   - 'path': the upload path will be `uploadBaseDir`/`uploadDir`/`field_name`/.
 *   - true: identical to 'filename'
 *   - false: don't use the field_name
 *   DEFAULTS TO: 'filename', except for models with an uuid id it's set to 'path'.
 * - useId: (boolean|string) include item id in the upload path or filename.
 *   Possible values:
 *   - 'filename': the item id is prefixed to the filename (`id`_`original filename`)
 *   - 'path': the upload path will be `uploadBaseDir`/`uploadDir`/`id`/. When the
 *     id is of an uuid type, there will be subdirectories: /aa/bb/aabbccdd-rest-of-uuid/.
 *   - true: identical to 'filename'
 *   - false: don't use the id
 *   DEFAULTS TO: 'filename', except for models with uuid id's it's set to 'path'
 * - callbacks: (array) list of functions to call on Model for certain actions.
 *   Currently available:
 *   - save: ($data) will call $Model->{$method}( $data, $fieldName, $relativePath, $fieldSettings ) and _must_ return $data
 *   - deleteFiles: (string) will call $Model->{$method}( $fieldName, $currentModelData, $fieldSettings )
 *   - moveUploadedFile: (string) will call $Model->{$method}( $fieldName, $relativePath, $fileName, $fieldSettings )
 *
 * Suggested field settings, overriding the settings above are:
 * - useFieldname (boolean|string)
 * - callbacks (array)
 *
 * @param Model $Model Model the behavior is being attached to.
 * @param array $config Array of configuration information.
 * @return void
 */
	public function setup(Model $model, $config = array()) {
		if( isset( $config['fields'] ) ) {
			$alias = $model->alias;

			$defaultSettings = array(
				'uploadBaseDir'   => (Configure::read('DkzrUtils.Fileupload.uploadBaseDir') ? Configure::read('DkzrUtils.Fileupload.uploadBaseDir') : IMAGES),
				'uploadDir'       => $model->name,
				'useId'           => 'filename',   // values: true|false|'path'|'filename'; true = 'filename'
				'useFieldname'    => 'filename',   // values: true|false|'path'|'filename'; true = 'filename'
				//'overwrite'     => true,
			);

			// test copied from Model::_isUUIDField()
			$primaryField = $model->schema( $model->primaryKey );
			if( $primaryField['length'] == 36 && in_array($primaryField['type'], array('string', 'binary')) ) {
				$defaultSettings['useId']        = 'path';
				$defaultSettings['useFieldname'] = 'path';
			}

			if( isset( $config['settings'] ) && is_array( $config['settings'] ) ) {
				$defaultSettings = Set::merge( $defaultSettings, $config['settings'] );
			}

			$this->_fileFields[$model->name] = array();
			foreach( (array) $config['fields'] as $field => $settings ) {
				if( is_string($settings) ) {
					$field = $settings;
					$settings = array();
				}
				$this->_fileFields[$model->name][$field] = Set::merge( $defaultSettings, array('field' => $field ), $settings );
			}
		}
	}

/**
 * Check for valid file upload.
 */
	public function beforeValidate(Model $model, $options = array()) {
		// TODO: check mimetype
		$alias = $model->alias;

		$return = true;
		foreach( $this->_fileFields[$model->name] as $field => $settings ) {
			if( !isset( $model->data[$alias][$field] ) ) {
				continue;
			}

			$file = $model->data[$alias][$field];
			if( is_array( $file ) ) {
				// expecting php $_FILES type array
				if(
					!isset( $file['tmp_name'] )
					|| !isset( $file['error'] )
					|| !in_array( $file['error'], array( UPLOAD_ERR_OK, UPLOAD_ERR_NO_FILE ) )
				) {
					if( isset($this->_uploadErrorTexts[$file['error']]) ) {
						$errorText = $this->_uploadErrorTexts[$file['error']];
					} else {
						$errorText = $this->_uploadErrorTexts['unknown'];
					}
					$model->invalidate( $field, $errorText );
					$return = false;
				}
			}
		}
		return $return;
	}

	public function beforeSave(Model $model, $options = array()) {
		$alias = $model->alias;

		$this->_uploadStack[$alias] = array();
		if (empty($model->data[$alias])) {
			return true;
		}

		$id = ( isset($model->data[$alias][$model->primaryKey]) ? $model->data[$alias][$model->primaryKey] : null );
		foreach( $this->_fileFields[$model->name] as $field => $settings ) {
			if( !isset( $model->data[$alias][$field] ) ) {
				continue;
			}
			$file = $model->data[$alias][$field];
			if( null !== $id && empty( $file ) ) {
				// empty input, remove file
				$this->deleteFiles( $model, $id, $field, array( 'save' => false ) );
			} else if( is_array( $file ) ) {
				// file upload, expecting $_FILES type array for $file
				if( isset( $file['tmp_name'], $file['error'] ) ) {
					if( UPLOAD_ERR_NO_FILE == $file['error'] ) {
						// no file upload, no changes: just unset the field.
						unset( $model->data[$alias][$field] );
					} else if( UPLOAD_ERR_OK == $file['error'] ) {
						// valid upload
						if( null === $id ) {
							// new Model item, move file after save
							$this->_uploadStack[$alias][$field] = array(
								'file'       => $file,
								'settings'   => $settings,
							);
							// temporarily set field value to $_FILES['tmp_name']
							$model->data[$alias][$field] = $file['tmp_name'];
						} else {
							$this->deleteFiles( $model, $id, $field, array( 'save' => false ) );
							$model->data[$alias][$field] = $this->_moveUploadedFile( $model, $field, $id, $file, $settings );
							if( isset($settings['callbacks']['save']) ) {
								$model->data[$alias] = call_user_func( array($model, $settings['callbacks']['save']), $model->data[$alias], $field, $model->data[$alias][$field], $settings );
							}
						}
					} else {
						/**
						 * Should not happen (should have been catched by beforeValidate).
						 */
						unset( $model->data[$alias][$field] );
					}
				}
			}
		}

		return true;
	}

/**
 * afterSave is only used for new entries, where the $model->id is not available on save()
 */
	public function afterSave(Model $model, $created, $options = array()) {
		$alias = $model->alias;
		if( isset($this->_uploadStack[$alias]) ) {
			$save = array();
			foreach( $this->_uploadStack[$alias] as $field => $item ) {
				$save[$field] = $this->_moveUploadedFile( $model, $field, $model->id, $item['file'], $item['settings'] );
				if( isset($item['settings']['callbacks']['save']) ) {
					$save = call_user_func( array($model, $item['settings']['callbacks']['save']), $save, $field, $save[$field], $item['settings'] );
				}
			}

			if( 0 < count( $save ) ) {
				$keepData = $model->data;
				$model->save( $save, array(
					'validate' => false,
					'callbacks' => false,
					'fieldList' => array_keys( $save )
				) );
				$model->data = Hash::merge($keepData, array($alias => $save));
			}
			unset($this->_uploadStack[$alias]);
		}

		return true;
	}

	public function beforeDelete(Model $model, $cascade = true) {
		if (empty($model->id)) {
			return false;
		}

		return $this->deleteFiles($model, $model->id);
	}

	public function cleanup(Model $model) {
		parent::cleanup( $model );

		if( isset($this->_fileFields[$model->name]) ) {
			unset($this->_fileFields[$model->name]);
		}

		if( isset($this->_uploadStack[$model->alias]) ) {
			unset($this->_uploadStack[$model->alias]);
		}
	}

	// public function beforeFind(Model $model, $query) { return true; }
	// public function afterFind(Model $model, $results, $primary) {}
	// public function afterValidate(Model $model) { return true; }
	// public function afterDelete(Model $model) {}

// MARK: public methods

/**
 * Delete all files associated with a record but do not delete the record.
 *
 * @param Model $model
 * @param int $id
 * @param array $fieldList
 * @return boolean
 */
	public function deleteFiles(Model $model, $id, $fieldList = array(), $options = array() ) {
		$alias = $model->alias;
		if( empty($fieldList) ) {
			$fieldList = array_keys( $this->_fileFields[$model->name] );
		}
		if( is_string( $fieldList ) ) {
			$fieldList = array( $fieldList );
		}

		if( 0 < count( array_diff( $fieldList, array_keys( $this->_fileFields[$model->name] ) ) ) ) {
			// invalid fields listed!
			return false;
		}

		$recursive = $model->recursive;
		$model->recursive = -1;
		$data = $model->findById( $id, array_merge($fieldList, array('id') ) );
		$model->recursive = $recursive;

		if (empty($data[$alias])) {
			return false;
		}

		foreach( $fieldList as $field ) {
			$settings = $this->_fileFields[$model->name][$field];
			if(
				isset( $data[$alias][$field] )
				&& is_file( $settings['uploadBaseDir'] . $data[$alias][$field] )
			) {
				if( unlink( $settings['uploadBaseDir'] . $data[$alias][$field] ) ) {
					$model->data[$model->alias][$field] = '';
				}
				if( isset($settings['callbacks']['deleteFiles']) ) {
					call_user_func( array($model, $settings['callbacks']['deleteFiles']), $field, $data[$alias], $settings);
				}
			}
		}

		return true;
	}

	public function getPathAndName(Model $model, $field, $id, $fileName, $settings = array()) {
		if( empty($settings) ) {
			$settings = $this->_fileFields[$model->name][$field];
		}

		$relativePath = $settings['uploadDir'] . DS;

		if( 'path' == $settings['useId'] ) {
			if( 36 == strlen( $id ) ) {
				$relativePath .= substr($id, 0, 2) . DS . substr($id, 2, 2) . DS . $id . DS;
			} else {
				$relativePath .= $id . DS;
			}
		}
		if( 'path' == $settings['useFieldname'] ) {
			$relativePath .= $field . DS;
		}

		App::uses('File', 'Utility');
		$file = new File($fileName);
		$name = str_replace('.', '-', $file->name()); // dots in filenames aren't always execpted (because of extensions)
		$ext  = $file->ext();

		if( 0 == strlen( $name ) || true === $settings['useFieldname'] || 'filename' == $settings['useFieldname'] ) {
			$name = $field;
		}
		if( true === $settings['useId'] || 'filename' == $settings['useId'] ) {
			$name = sprintf( '%s_%s', $id, $name );
		}
		$fileName = $name . ( 0 < strlen($ext) ? '.' . $ext : '' );

		return compact( 'relativePath', 'fileName' );
	}

// MARK: protected & private methods

/**
 * Move uploaded file
 *
 * @return string filepath relative to `uploadBaseDir`
 */
	protected function _moveUploadedFile( $model, $field, $id, $file, $settings ) {
		extract( $this->getPathAndName( $model, $field, $id, $file['name'], $settings ) );

		if( !is_dir( $settings['uploadBaseDir'] . $relativePath ) ) {
			$old = umask(0);
			mkdir( $settings['uploadBaseDir'] . $relativePath, 0777, true );
			umask($old);
		}
		move_uploaded_file( $file['tmp_name'], $settings['uploadBaseDir'] . $relativePath . $fileName );

		if( isset($settings['callbacks']['moveUploadedFile']) ) {
			call_user_func( array($model, $settings['callbacks']['moveUploadedFile']), $field, $relativePath, $fileName, $settings );
		}
		return $relativePath . $fileName;
	}
}
